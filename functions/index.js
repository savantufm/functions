const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
const db = admin.firestore();

exports.newUser = functions.auth.user().onCreate((user) => {
	console.log(user.uid);
	return db.collection("users").doc(user.uid).set({
		name: user.displayName,
		email: user.email ? user.email : '<unknown>',
		photo: user.photoURL ? user.photoURL : '<unknown>'
	});
});

exports.welcomeNotification = functions.firestore.document('/fcm_tokens/{new}').onCreate((snap, context) => {
	const payload = {
		notification: {
			title: '¡Bienvenido a SAVANT!',
		},
		token: snap.data().token
	};
	return admin.messaging().send(payload).then(response => {
		console.log('Sucessfully notification', response);
		return true;
	}).catch(error => {
		console.error('Notification failed', error);
	});
});